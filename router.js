const express = require("express");
const router = express.Router();
const Document = require("./Document");

router.get("/api/", (req, res) => {
  res.send("Dox server is up and running!!!");
});
router.post("/api/rename", async (req, res) => {
  const { newName, currentName } = req.body;
  const doc = await Document.findOneAndUpdate(
    { docId: currentName },
    { docId: newName },
    {
      new: true,
      //   runValidators: true,
    }
  );
  //   if (!doc) {
  //     return next(
  //       new AppErrorHandler(`No document found with ID {${req.params.id}}`, 404)
  //     );
  //   }

  res.status(200).json({
    status: "success",
    data: {
      data: doc?.docId,
    },
  });

  //   Document.findOneAndUpdate(
  //     { newName },
  //     { $set: { docId } },
  //     { new: true },
  //     (err, doc) => {
  //       if (err) {
  //         console.error(err);
  //         res.send("Error");
  //       } else {
  //         console.log(doc);
  //         res.send("Success");
  //       }
  //     }
  //   );

  //   res.status(201).json({
  //     status: "success",
  //     data: {
  //       data: req.body,
  //     },
  //   });
});

module.exports = router;
